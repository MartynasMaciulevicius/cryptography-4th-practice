/**
 * @author Martynas Maciulevičius.
 * @version 1.0 2015-11-06
 */
object Kuprin4 {
  val pubKey = Array(93657, 181853, 186404, 65054, 76627, 150957, 103679, 160638)
  val keyFirst = 1519
  val module = 240101
  val cipher = Array(433311, 536990, 528895, 519214, 613617, 605522, 528895, 186404, 783531, 679852, 536990, 528895, 186404, 471936, 744906, 697628, 186404, 783531, 679852, 536990, 528895, 186404, 433311, 528895, 471936, 528895, 605522, 783531, 528895)
  lazy val defaultS = 5540
  lazy val privateKey = getPrivateWeights()
  lazy val cipherStar = getCipher()

  //  def getS(pubKey: Array[Int] = pubKey, privKeyFirst: Int = keyFirst, module: Int = module): Int =
  //    Math.floorMod(privKeyFirst / pubKey.head, module)

  def main(args: Array[String]): Unit = {
    val weights = privateKey
    getCipher().map(i => unpack(i, weights)).map(_.toChar).foreach(print)
  }

  def getCipher(sValue: Int = defaultS, cipher: Array[Int] = cipher, module: Int = module): Array[Int] = {
    cipher.map(item => Math.floorMod(item * sValue, module))
  }


  def unpack(backpack: Int, weights: Array[Int]): Int = {
    case class DecodedBackpack(backpack: Int, mask: Int)
    weights.reverse.foldLeft(DecodedBackpack(backpack, 0))((pack: DecodedBackpack, weight: Int) => {
      //      println(weight, pack)
      val newMask = pack.mask << 1
      if (pack.backpack >= weight)
        DecodedBackpack(pack.backpack - weight, newMask | 1)
      else
        DecodedBackpack(pack.backpack, newMask)
    })
      .mask
  }

  def getPrivateWeights(pubKey: Array[Int] = pubKey, sValue: Int = defaultS): Array[Int] = {
    pubKey.map(item => Math.floorMod(item * sValue, module))
  }

}
