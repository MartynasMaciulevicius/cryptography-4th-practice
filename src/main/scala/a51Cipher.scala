import StreamCipher._

/**
 * @author Martynas Maciulevičius.
 * @version 1.0 2015-10-09
 */
object a51Cipher {

  val defaultMask = 0xff
  val defaultBase = 8
  val cipher = Array(88, 123, 22, 35, 6, 243, 1, 214, 115, 54, 166, 135, 230, 9, 216, 118, 61, 185, 159, 238, 15, 200, 105, 63, 160, 157, 242, 11, 212, 126, 53, 164, 146, 244, 7, 207, 108, 61, 186, 152, 230, 29, 203, 115, 38, 186, 135, 230, 26, 200, 117, 63, 166, 131, 230, 0, 210, 104, 61, 188, 131, 245, 7, 216, 110, 53, 162, 150, 235, 7, 210, 113, 33, 185, 128, 243, 15, 206, 113, 33, 187, 129, 230, 10, 220, 111, 36, 187, 154, 245, 7, 206, 110, 53, 186, 128, 236, 28, 200, 96, 48, 172, 128)

  def getBitStream(cVal: Int, initVector: Int): Stream[Boolean] = {
    Stream
      .iterate((initVector, initVector, initVector))((reg: (Int, Int, Int)) =>
        rotateAllIfNeeded(
          extractBitsBigEndian(
            reg,
            (1, 2, 3)),
          reg))
      .map((reg: (Int, Int, Int)) =>
        nextBits(cVal, reg))
  }

  def nextBits(cVal: Int, reg: (Int, Int, Int)): Boolean = {
    produceStreamBit(cVal, reg._1) ^
      produceStreamBit(cVal, reg._2) ^
      produceStreamBit(cVal, reg._3)
  }

  def extractBitsBigEndian(reg: (Int, Int, Int), clockBits: (Int, Int, Int)): (Boolean, Boolean, Boolean) = (
    extractBitBigEndianAsBool(reg._1, clockBits._1),
    extractBitBigEndianAsBool(reg._2, clockBits._2),
    extractBitBigEndianAsBool(reg._3, clockBits._3))

  def rotateAllIfNeeded(regRotations: (Boolean, Boolean, Boolean), reg: (Int, Int, Int)): (Int, Int, Int) = {
    val bool = computeRepeatedBoolean(regRotations)
    val reg1 = if (regRotations._1 == bool) rotateRegister(regRotations._1, reg._1) else reg._1
    val reg2 = if (regRotations._2 == bool) rotateRegister(regRotations._2, reg._2) else reg._2
    val reg3 = if (regRotations._3 == bool) rotateRegister(regRotations._3, reg._3) else reg._3
    (reg1, reg2, reg3)
  }

  def computeRepeatedBoolean(bools: (Boolean, Boolean, Boolean)): Boolean =
    if (bools._1 == bools._2 || bools._1 == bools._3)
      bools._1
    else
      !bools._1

  def rotateRegister(insertedBit: Boolean, reg: Int, mask: Int = defaultMask): Int =
    reg << 1 & mask | (if (insertedBit) 1 else 0)

  def extractBitBigEndian(reg: Int, index: Int, base: Int = defaultBase): Int =
    reg >> base - 1 - index & 1 // indexing from 0

  def extractBitBigEndianAsBool(reg: Int, index: Int, base: Int = defaultBase): Boolean =
    extractBitBigEndian(reg, index, base) == 1

  def getByteStream(bitStream: Stream[Boolean]): Stream[Int] = {
    Stream.iterate((bitStream, 0))((tuple: (Stream[Boolean], Int)) =>
      produceStreamByte(tuple._1))
      .map(_._2)
  }

  def produceStreamByte(bitStream: Stream[Boolean], base: Int = defaultBase, mask: Int = defaultMask): (Stream[Boolean], Int) = {
    val parts = bitStream.splitAt(base - 1)
    val out = boolStreamToInt(parts._1).foldLeft(0)((prev: Int, bit: Int) => prev << 1 | bit)
    (parts._2, out & mask)
  }

  implicit def boolStreamToInt(str: Stream[Boolean]): Stream[Int] =
    str.map(if (_) 1 else 0)

}
