import org.apache.commons.math3.linear.{Array2DRowRealMatrix, ArrayRealVector, LUDecomposition, RealVector}

import scala.collection.GenIterable

/**
 * @author Martynas Maciulevičius.
 * @version 1.0 2015-10-02
 */
object StreamCipher {

  val firstLetters = "VI"
  val cipher = Stream(94, 131, 171, 19, 5, 150, 107, 242, 68, 193, 185, 199, 253, 193, 49, 55, 98, 121, 162, 80, 30, 65, 153, 171, 15, 22, 151, 96, 247, 68, 220, 160, 201, 253, 210, 33, 61, 118, 108, 178, 80, 30, 93, 153, 172, 15, 7, 144, 115, 250, 84, 223, 165, 223, 226, 205, 54, 55, 98, 120, 160, 77, 12, 65, 142, 173, 21, 3, 131, 97, 235, 92, 192, 162, 193, 238, 207, 36, 56, 113, 106, 172, 80, 30, 65, 135, 177, 3, 16, 141, 102, 247, 95, 208, 180, 217, 224, 192, 36)

  def getTwoKeyBytes(twoLetters: String, twoCipherDigits: GenIterable[Int]) =
    twoLetters
      .zip(twoCipherDigits)
      .map(letterAndCipher => letterAndCipher._1.toInt ^ letterAndCipher._2)

  def intToBitArray(int: Int, length: Int): Traversable[Int] =
    (0 until length)
      .map(i =>
        (int >> i) & 1)

  def findFirstTwoKeyValues(twoLetters: String, twoCipherDigits: GenIterable[Int], numberBits: Int): Traversable[Int] =
    getTwoKeyBytes(twoLetters, twoCipherDigits)
      .map(intToBitArray(_, numberBits))
      .map(_.toStream)
      .reduce(_.append(_))

  def createLinearSystem(keys: Traversable[Int], equationCount: Int): Traversable[Traversable[Int]] =
    (0 to equationCount - 1).toStream
      .map(i => keys.slice(i, equationCount + i + 1))

  /**
   * Input: Array of arrays:
   * Output: roots:
   * example:
   * Input:
   * {
   * {1 0 3},
   * {0 1 6}
   * }
   * Output:
   * {3 6}
   */
  def solveLinearSystem(system: Array[Array[Double]]): Array[Double] = {
    val sides = system.map(i => i.splitAt(i.length - 1))

    val matrix = new Array2DRowRealMatrix(sides.map(_._1))
    val coefficients = new ArrayRealVector(sides.map(_._2(0)), false)
    val solved: RealVector = new LUDecomposition(matrix).getSolver.solve(coefficients)
    solved.toArray
  }

  def solveLinearSystem(system: Traversable[Traversable[Int]]): Traversable[Int] =
    solveLinearSystem(
      system.map(_.map(_.toDouble).toArray)
        .toArray)
      .map(_.toInt)

  def calculateCValues(firstLetters: String = firstLetters, cipher: GenIterable[Int] = cipher, numOfBits: Int = 8): Traversable[Int] =
    solveLinearSystem(
      createLinearSystem(
        findFirstTwoKeyValues(firstLetters, cipher, numOfBits), numOfBits))
      .toList
      .reverse

  def bitArrayAsInt(bits: Traversable[Int]): Int =
    bits.toList
      .reverse
      .fold(0)((base, bit) =>
        (base << 1) | bit)

  def produceStreamBit(cValue: Int, registerValues: Int): Boolean = {
    val out = xorAllBits(registerValues & cValue)
    //    println(cValue.toFixedBinaryString + " + " + registerValues.toFixedBinaryString + " = " + (registerValues & cValue).toFixedBinaryString + " -> " + out)
    out
  }

  class FixedBitInt(int: Int) {
    def toFixedBinaryString: String = {
      val str = int.toBinaryString
      ("00000000" + str).substring(str.length)
    }
  }

  implicit def intToFixed(int: Int): FixedBitInt = new FixedBitInt(int)

  def xorAllBits(int: Int): Boolean =
    (Integer.bitCount(int) % 2) == 1

  //    intToBitArray(int, length).reduce(_ ^ _)

  def produceStreamByte(cValue: Int, registerValues: Int, byteLength: Int, byteMask: Int): Int = {
    (0 until byteLength).fold(registerValues)((reg: Int, unused: Int) => {
      reg << 1 & byteMask | (if (produceStreamBit(cValue, reg)) 1 else 0)
    })
  }

  def getDefaultCValue(byteLength: Int): Int =
    bitArrayAsInt(calculateCValues(numOfBits = byteLength).toArray)

  def produceStream(cValue: Int, registerValues: Int, byteLength: Int): Stream[Int] =
    Stream.iterate(registerValues)(produceStreamByte(cValue, _, byteLength, 0xff))

  def encode(plaintext: Stream[Int], key: Stream[Int]): Stream[Int] =
    plaintext.zip(key).map((ints: (Int, Int)) => ints._1 ^ ints._2)

  def producePartOneStream(): Stream[Int] = {
    produceStream(Integer.parseInt("01111101".reverse, 2) /*,getDefaultCValue(8)*/ , getTwoKeyBytes(firstLetters, cipher).head, 8)
  }

  def produceBruteForceStreamOfStreams: Stream[Stream[Int]] = {
    val registers = getTwoKeyBytes(firstLetters, cipher).last
    (0 until (Math.pow(2, 8).toInt - 1))
      .toStream
      .map(produceStream(_, registers, 8))
  }

  def decodePartOne(byteLength: Int): Traversable[Int] =
    cipher.zip(producePartOneStream())
      .map((tuple: (Int, Int)) => tuple._1 ^ tuple._2)

  def main(args: Array[String]) {
    println(
      decodePartOne(8).map(_.toChar + " ").toList)
  }

}
