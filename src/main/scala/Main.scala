import scala.collection.GenTraversableOnce
import scala.language.implicitConversions

/**
 * @author Martynas Maciulevičius.
 * @version 1.0 2015-09-25
 */
object Main {
  val key = List(97, 114, 103)
  val cipherECB = List((80, 31), (243, 86), (50, 75), (3, 193), (50, 75), (216, 82), (155, 106), (143, 92))
  val cipherCBC = List((147, 20), (51, 98), (73, 209), (106, 243), (78, 164), (226, 255), (148, 210), (159, 98), (22, 71), (48, 201), (64, 77))
  val cipherOFB = List((230, 38), (62, 186), (189, 45), (173, 184), (68, 28), (172, 66), (64, 237), (180, 73), (56, 175), (43, 88), (125, 238), (185, 152), (199, 199), (73, 36), (251, 193), (16, 98), (100, 152))
  val cbcIV = "ai"
  val ofbIV = "ne"

  def add(a: Int*): Int = {
    val out = Math.floorMod(a.sum, 256)
    //    println("add: " + a + " -> " + out)
    out
  }

  def xor(a: Traversable[Int]): Int = {
    val out = Math.floorMod(a.reduce(_ ^ _), 256)
    //    println("xor: " + a + " -> " + out)
    out
  }

  def rotateLeft(c: Int, bits: Int): Int = {

    val out = Math.floorMod((c << bits) | (c >>> (8 - bits)), 256)
    //    println("ROL: " + c + " -> " + out)
    out
  }

  def rotateRight(c: Int, bits: Int): Int = {
    val out = Math.floorMod((c >>> bits) | (c << (8 - bits)), 256)
    //    println("ROR: " + c + " -> " + out)
    out
  }

  def inner(left: Int, key: List[Int]): Int = {
    val data = Seq(
      add(left, key.head),
      add(rotateLeft(left, 2), key(1)),
      add(rotateRight(left, 2), key(2)))
    val xorOut = xor(data)
    //    println("left: " + left + " " + "inner: " + data + " -> " + xorOut)
    xorOut
  }

  def cipherIteration(left: Int, right: Int, key: List[Int]): (Int, Int) =
    (add(right, inner(left, key)), left)

  def decipherIteration(left: Int, right: Int, key: List[Int]): (Int, Int) = {
    (right, Math.floorMod(left - inner(right, key), 256))
  }

  def threeIterations(left: Int, right: Int)(func: (Int, Int, List[Int]) => (Int, Int))(key: Seq[List[Int]]): (Int, Int) =
    key
      .foldLeft[(Int, Int)]((left, right))((lr, bytes) => {
      val out = func(lr._1, lr._2, bytes)
//      println(out)
      out
    })

  def getRotations(key: List[Int]): Seq[List[Int]] =
    key.indices
      .map(key.splitAt)
      .map(tuple => tuple._2 ::: tuple._1)

  def decryptECB(cipher: List[(Int, Int)], key: List[Int]): List[(Int, Int)] =
    cipher
      .map(tuple =>
        threeIterations(tuple._2, tuple._1)(decipherIteration)(getRotations(key).reverse))

  def decryptDefaultECB(): List[Int] = decryptECB(cipherECB, key).flatten

  def decryptCBC(cipher: Traversable[(Int, Int)], key: List[Int], initVector: (Int, Int)): Stream[Int] = {
    cipher.toStream
      .zip(Stream
        .concat(List(initVector), cipher))
      .flatMap(cbcIteration)
  }

  def cbcIteration(tuple: ((Int, Int), (Int, Int))): (Int, Int) = {
    val out = threeIterations(tuple._1._2, tuple._1._1)(decipherIteration)(getRotations(key).reverse)
    (out._1 ^ tuple._2._1, out._2 ^ tuple._2._2)
  }

  def decryptDefaultCBC(): Stream[Int] =
    decryptCBC(cipherCBC, key, (cbcIV.charAt(0).toInt, cbcIV.charAt(1).toInt))

  implicit def flattenTuples[A](tuple: (A, A)): GenTraversableOnce[A] = Iterator(tuple._1, tuple._2)

  def decryptOFB(cipher: Traversable[(Int, Int)], key: List[Int], initVector: (Int, Int)): Stream[Int] = {
    cipher.toStream
      .flatten
      .zip(getOfbStream(initVector, key))
      .map((item: (Int, Int)) => item._1 ^ item._2)
  }

  def getOfbStream(initVector: (Int, Int), key: List[Int]): Stream[Int] = {
    Stream
      .iterate(initVector)((tuple: (Int, Int)) =>
        threeIterations(tuple._1, tuple._2)(cipherIteration)(getRotations(key)))
      .drop(1)
      .flatten
  }

  def decryptDefaultOFB(): Stream[Int] =
    decryptOFB(cipherOFB, key, (ofbIV.charAt(0).toInt, ofbIV.charAt(1).toInt))

  def main(args: Array[String]) {
    println(
      decryptECB(cipherECB, key)

        .flatten.map(_.toChar)
        .foldLeft(new StringBuffer())((buffer, c) => buffer.append(c)))

    println(
      decryptDefaultCBC()
        .toArray
        .map(_.toChar)
        .foldLeft(new StringBuffer())((buffer, c) => buffer.append(c)))
  }

}
